﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkiPlayer : MonoBehaviour 
{
	public Transform _player;
	public Rigidbody _rb;
	[Tooltip("The location of the player's feet, basically the bottom of the bounding box, used for logic/effects")]
	public Transform _playerBottom;
	[SerializeField]
	Camera cam;
	Quaternion camInitialRotation;
	[SerializeField]
	Transform _hudTransform;
	float camInfluence;
	float camInfluenceX;

	public float _maxRotSpeed; 
	public float _maxMoveSpeed;
	float rotSpeed = 0.0f;
	float moveSpeed = 0.0f;
	//float friction = 0.01f;
	public AnimationCurve _accelRotCurve;
	public AnimationCurve _accelMoveCurve;
	public float _jumpLength = 0.3f;
	float jumpTime = 0f;
	public AnimationCurve _jumpCurve;
	[Tooltip("0 is instant stop, closer to 1 takes longer to brake"), Range(0,1)]
	public float _brakeForce = 0.96f;

	public GameObject _driftEffect;
	public GameObject _driftFlameEffect;
	GameObject _driftFlameEffectReference;
	ParticleSystem _driftParticles;
	public GameObject _jumpEffect;
	public GameObject _speedEffect;
	ParticleSystem _speedParticles;

	[SerializeField]
	GameObject _explodePart;

	bool isDrifting = false;
	bool isJumpCurve = false;
	bool isOnSurface = true;
	bool isInputJumping = false;
	bool isInputJumpingRelease = false;
	bool isInputBraking = false;
	bool isInputAccelerating = false;
	bool isInputGroundPound = false;
	bool isInputTurningLeft = false;
	bool isInputTurningRight = false;


	// Use this for initialization
	void Start () 
	{
		if(_player == null)
		{
			_player = transform;
		}
		if(_rb == null)
		{
			_rb = _player.GetComponent<Rigidbody>();
		}
		if(_playerBottom == null)
		{
			Debug.LogWarning("Player feet not set!");
		}
		if(cam == null)
		{
			cam = GetComponentInChildren<Camera>();
		}
		camInitialRotation = cam.transform.localRotation;
		_driftParticles = Instantiate(_driftEffect, _rb.transform).GetComponent<ParticleSystem>();
		_speedParticles = Instantiate(_speedEffect, _rb.transform).GetComponent<ParticleSystem>(); 
	}
	
	// Update is called once per frame
	void Update () 
	{
		Debug.Log("Speed:\t" + _rb.velocity.magnitude);

		if(Input.GetKey(KeyCode.A))
		{
			isInputTurningLeft = true;
		}
		if(Input.GetKey(KeyCode.D))
		{
			isInputTurningRight = true;
		}
		if(Input.GetKey(KeyCode.W))
		{
			isInputAccelerating = true;
			MovePlayer();
		}
		if(Input.GetKey(KeyCode.S))
		{
			isInputBraking = true;
			BrakePlayer();
		}
		if(Input.GetKey(KeyCode.Space))
		{
			isInputJumping = true;
		}
		if(Input.GetKeyUp(KeyCode.Space))
		{
			isInputJumpingRelease = true;
		}
		if(Input.GetKeyDown(KeyCode.LeftShift))
		{
			isInputGroundPound = true;
		}
		if(Input.GetKeyDown(KeyCode.RightShift))
		{
			Explode();
		}


		

		if(_rb.velocity.magnitude > 10.0f)
		{
			if(!_speedParticles.isPlaying)
			{
				//_speedParticles.Play();
			}
		}
		else
		{
			_speedParticles.Stop();
		}
		
		float dotProduct = Vector3.Dot(_rb.transform.forward, _rb.velocity.normalized);
		Vector3 velocityXZ = _rb.velocity;
		velocityXZ.y -= velocityXZ.y;
		if(isOnSurface && velocityXZ.magnitude > Mathf.Lerp(1f, 40f, Mathf.InverseLerp(0.2f, 0.9f, dotProduct)) && dotProduct < 0.95f)
		{
			if(!isDrifting)
			{
				isDrifting = true;
				_driftFlameEffectReference = Instantiate(_driftFlameEffect, _playerBottom);
				//_driftParticles.Play();
			}
		}
		else
		{
			isDrifting = false;
			if(_driftFlameEffectReference != null && _driftFlameEffectReference.transform.parent != null)
			{
				_driftFlameEffectReference.transform.parent = null;
				//_driftFlameEffectReference = null;
			}
			//_driftParticles.Stop();
		}

	}

	private void FixedUpdate()
	{
		if(isInputTurningLeft)
		{
			isInputTurningLeft = false;
			RotatePlayer(-1f);
		}

		if(isInputTurningRight)
		{
			isInputTurningRight = false;
			RotatePlayer(1f);
		}

		if(isInputAccelerating)
		{
			isInputAccelerating = false;
			MovePlayer();
		}

		if(isInputBraking)
		{
			isInputBraking = false;
			BrakePlayer();
		}

		if(isInputJumping)
		{
			isInputJumping = false;
			Jump();
		}
		
		if(isJumpCurve)
		{
			JumpUpdate();
		}

		if(isInputGroundPound)
		{
			GroundPound();
		}

		float newCamValue = (Quaternion.Inverse(cam.transform.rotation) * _rb.velocity.normalized).x * 
			Mathf.InverseLerp(0f, 50f, Mathf.Abs((Quaternion.Inverse(cam.transform.rotation) * _rb.velocity).x)); 
		camInfluence = Mathf.Lerp(camInfluence, newCamValue, 0.05f);
		//Debug.Log("Mag: " + _rb.velocity.magnitude + "\nOffset: " + newCamValue);

		newCamValue = (Quaternion.Inverse(cam.transform.rotation) * _rb.velocity.normalized).z * 
			Mathf.InverseLerp(0f, 10f, Mathf.Abs((Quaternion.Inverse(cam.transform.rotation) * _rb.velocity).z)); 
		camInfluenceX = Mathf.Lerp(camInfluenceX, newCamValue, 0.05f);
		
		cam.transform.localRotation = camInitialRotation * Quaternion.Euler(-camInfluenceX * 10f, camInfluence * 35f, 0f);
		_hudTransform.localRotation = Quaternion.Euler(camInfluenceX * 5f, -camInfluence * 7f, 0f);
		//_Player.transform.Rotate(0.0f, rotSpeed, 0.0f);
	}

	void MovePlayer()
	{
		if(isOnSurface)
		{
			moveSpeed = _rb.velocity.magnitude;
			
			if(moveSpeed > _maxMoveSpeed)
			{
				_rb.velocity = _rb.velocity / (_rb.velocity.magnitude / _maxMoveSpeed);
			}

			_rb.AddForce((_player.transform.forward * _accelMoveCurve.Evaluate(moveSpeed / _maxMoveSpeed) +
				_player.transform.forward * Mathf.InverseLerp(1f, -1f, Vector3.Dot(_player.transform.forward, _rb.velocity.normalized))) * 20.0f);
			
		}
	}

	void BrakePlayer()
	{
		if(isOnSurface)
		{
			_rb.velocity *= _brakeForce;
		}
	}

	void RotatePlayer(float direction)
	{
		if(!isOnSurface)
		{
			rotSpeed = _rb.angularVelocity.y;
			_rb.AddTorque(0.0f, direction * _accelRotCurve.Evaluate(Mathf.Abs(rotSpeed / _maxRotSpeed)), 0.0f);
			if(direction > 0f && rotSpeed < 0)
			{
				_rb.AddTorque(0.0f, direction * 1f, 0.0f);
			}
			else if(direction < 0f && rotSpeed > 0)
			{
				_rb.AddTorque(0.0f, direction * 1f, 0.0f);
			}
			Vector3 angVel = _rb.angularVelocity;
			angVel.y = Mathf.Clamp(angVel.y, -_maxRotSpeed, _maxRotSpeed);
			_rb.angularVelocity = angVel;
			//Debug.Log(rotSpeed);
		}
	}

	void Jump()
	{
		if(isOnSurface)
		{
			_rb.AddForce(new Vector3(0f, 3f, 0f), ForceMode.Impulse);
			isOnSurface = false;
			isJumpCurve = true;
			jumpTime = 0f;
			Instantiate(_jumpEffect, _rb.transform);
			//_driftParticles.Stop();
			isInputJumpingRelease = false;
		}
	}

	void GroundPound()
	{
		if(!isOnSurface)
		{
			Vector3 newVel = _rb.velocity;
			newVel.y = -30.0f;
			_rb.velocity = newVel;
			//_rb.AddForce(new Vector3(0f, -50f, 0f), ForceMode.Impulse);
			isInputGroundPound = false;
		}
	}

	void JumpUpdate()
	{
		jumpTime += Time.fixedDeltaTime;		
		_rb.AddForce(new Vector3(0f, _jumpCurve.Evaluate(jumpTime / _jumpLength), 0f), ForceMode.Impulse);
		if(isInputJumpingRelease || jumpTime > _jumpLength)
		{
			isJumpCurve = false;
			isInputJumpingRelease = false;
		}
	}

	private void OnCollisionEnter(Collision other)
	{
		isOnSurface = true;
	}

	void Explode()
	{
		for(int i = 0; i < 10; ++i)
		{
			for(int j = 0; j < 10; ++j)
			{
				for(int k = 0; k < 10; ++k)
				{
					Rigidbody explod = 
					Instantiate(_explodePart, transform.position + new Vector3(i*0.1f-0.5f, j*0.1f-0.5f, k*0.1f-0.5f), transform.rotation).GetComponent<Rigidbody>();
					explod.velocity = _rb.velocity;
				}
			}
		}
		cam.transform.parent = null;
		Destroy(gameObject);
	}
}
