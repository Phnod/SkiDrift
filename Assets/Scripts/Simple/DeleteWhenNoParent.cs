﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteWhenNoParent : MonoBehaviour 
{
	bool activated = false;
	public float _lifetime = 5f; 
	// Use this for initialization
	void Update () 
	{
		if(!activated && transform.parent == null)
		{
			Destroy(this.gameObject, _lifetime);
		}		
	}
}
