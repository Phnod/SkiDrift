﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteOnLifetime : MonoBehaviour 
{
	public float _lifetime = 5f; 
	// Use this for initialization
	void Start () 
	{
		Destroy(this.gameObject, _lifetime);
	}
}
