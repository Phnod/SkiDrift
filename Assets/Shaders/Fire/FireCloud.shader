﻿Shader "Stephen/FireCloud"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_CloudTex ("Cloud Texture", 2D) = "white" {}
		_AlphaCutoff ("Alpha Cutoff", Range (0.0, 1.0)) = 0.5
		_TimeSpeed ("Cloud Speed", Float) = 1.0
		_CloudCutoff ("Cloud Cutoff", Float) = 0.2
		[HDR] _Color ("Color", Color) = (1, 1, 1, 1)
		_InvFade ("Soft Factor", Range(0.01, 3.0)) = 1.0

		[HideInInspector] _OffsetAmount("Offset Amount", Float) = 0
		[HideInInspector] _OffsetAngle("Offset Angle", Float) = 0

		[HideInInspector] _SrcBlend ("_SrcBlend", Int) = 1
		[HideInInspector] _DstBlend ("_DstBlend", Int) = 0
		
		[HideInInspector] _ZWrite ("_ZWrite", Int) = 1
		[HideInInspector] _Cull ("_Cull", Int) = 2

		[HideInInspector] _BlendSetting("Blend Setting", Int) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }

		LOD 100
		Blend [_SrcBlend] [_DstBlend]
		ZWrite [_ZWrite]
		Cull [_Cull]
		Offset [_OffsetAngle], [_OffsetAmount]


		Pass
		{
			CGPROGRAM
			#include "FireInclude.cginc"
			#pragma vertex vertFire
			#pragma fragment fragFire
			// make fog work
			#pragma multi_compile_fog

			#pragma shader_feature _ _RENDERING_CUTOUT _RENDERING_FADE _RENDERING_TRANSPARENT
			#pragma shader_feature _FIRE_DARK_EDGE
			#pragma shader_feature _FIRE_SOFT
			#pragma shader_feature _FIRE_TEST		
			
			ENDCG
		}
	}
	CustomEditor "FireShaderGUI"
}
