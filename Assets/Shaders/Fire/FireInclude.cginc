﻿#if !defined(SAT_FIRE_INCLUDED)
#define SAT_FIRE_INCLUDED

#include "UnityCG.cginc"

sampler2D _MainTex;
float4 _MainTex_ST;
sampler2D _CloudTex;
float4 _CloudTex_ST;
float4 _Color;
float _CloudCutoff;
float _TimeSpeed;
float _AlphaCutoff;
sampler2D_float _CameraDepthTexture;
float _InvFade;

struct appdata
{
	float4 vertex : POSITION;
	float4 color : COLOR;
	float4 uv : TEXCOORD0;
};

struct v2f
{
	float4 uv : TEXCOORD0;
	UNITY_FOG_COORDS(1)
	float4 color : COLOR;
	float4 vertex : SV_POSITION;
	//#ifdef SOFTPARTICLES_ON
	#if defined(_FIRE_SOFT)
		float4 projPos : TEXCOORD2;
	#endif
};


v2f vertFire (appdata v)
{
	v2f o;
	o.vertex = UnityObjectToClipPos(v.vertex);
	o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
	o.uv.zw = TRANSFORM_TEX(v.uv, _CloudTex) + v.uv.wz;
	UNITY_TRANSFER_FOG(o,o.vertex);
	o.color = v.color;
	//#ifdef SOFTPARTICLES_ON
	#if defined(_FIRE_SOFT)
		o.projPos = ComputeScreenPos (o.vertex);
		COMPUTE_EYEDEPTH(o.projPos.z);
	#endif
	return o;
}

float4 fragFire (v2f i) : SV_Target
{
	float cloudSum;
	float4 cloud = 0;
	cloud.r = tex2D(_CloudTex, i.uv.zw + float2( _Time.x * 0.3,  _Time.x * 5.0) * _TimeSpeed).r;
	cloud.g = tex2D(_CloudTex, i.uv.zw + float2( _Time.x * 0.3, -_Time.x * 5.0) * _TimeSpeed).g;
	cloud.b = tex2D(_CloudTex, i.uv.zw + float2(-_Time.x * 5.0,  _Time.x * 0.3) * _TimeSpeed).b;
	cloud.a = tex2D(_CloudTex, i.uv.zw + float2(-_Time.x * 5.0, -_Time.x * 0.3) * _TimeSpeed).a;
	cloudSum = cloud.r + cloud.g + cloud.b + cloud.a;
	cloudSum *= 0.25;
	cloudSum -= _CloudCutoff;

	// sample the texture
	float4 col = tex2D(_MainTex, i.uv) * _Color * i.color;

	

	#if defined(_RENDERING_CUTOUT)		
		clip(col.a - cloudSum - _AlphaCutoff);
	#else
		col.a = saturate(col.a - cloudSum);
	#endif
	#if defined(_FIRE_DARK_EDGE)
		col.rgb *= saturate(col.a - cloudSum * 1.0);
	#endif

	//#ifdef SOFTPARTICLES_ON
	#if defined(_FIRE_SOFT)
		float sceneDepth = LinearEyeDepth (SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
		float projZ = i.projPos.z;
		float fade = saturate (_InvFade * (sceneDepth-projZ));
		col.a *= fade;
	#endif
	// apply fog
	UNITY_APPLY_FOG(i.fogCoord, col);
	return col;
}
#endif